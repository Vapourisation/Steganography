import cryptography
import cv2
import hashlib
import json
import logging
import math
import numpy as np
import os
import time

from matplotlib import pyplot as plt
from PIL import Image
from PIL.ExifTags import TAGS

from Encryption import Encrypt

logging.basicConfig(
    format="%(asctime)s %(name)-30s %(levelname)-8s %(message)s",
    datefmt='%H:%M:%S',
    level=logging.DEBUG
)


class BaseSteganography(object):
    def __init__(self, cache=None, image_path='', text='', integrity=1):
        """
        :arg image_path File path to the image to be decoded/encoded
        :arg text The text to be encoded into the image
        :arg cache Redis cache object

        Sets the defaults for the arguments used throughout the Class. These can be empty and will need to be defined
        later on. So if image_path is empty here then it'll need to be set later on, the same with text.
        """
        self.image_path = image_path
        self.text = text
        self.cache = cache
        self.logger = logging.getLogger('steganography')
        self.integrity = int(integrity)
        self.start_time = time.time()
        self.start_point = '+++++++'
        self.end_point = '########'

    def image_factor(self, w: int, h: int) -> int:
        """
        Takes the width and height of an image, and the requested integrity, and determines the factor by which to alter
        the rate of encoding.
        :param w: Width of the image in pixels
        :param h: height of the image in pixels
        """

        percentage = math.ceil((h * (10 * self.integrity)) / 100)

        self.logger.debug(f'INTEGRITY: {self.integrity}')
        self.logger.debug(f'PERCENTAGE: {percentage}')

        if w <= 1024:
            factor = (h / percentage) * 2
        elif 1024 < w <= 1280:
            factor = (h / percentage) * 5
        elif 1280 < w <= 1920:
            factor = (h / percentage) * 10
        elif 1920 < w <= 2560:
            factor = (h / percentage) * 20
        elif 2560 < w <= 3840:
            factor = (h / percentage) * 40
        else:
            factor = (h / percentage) * 80

        factor = math.ceil(factor)

        return factor

    def gen_data(self, data):
        """
        Convert encoding data into 8-bit binary form using ASCII value of characters
        :param data: The data to be converted to 8-bit binary
        """

        new_data = []

        for i in data:
            new_data.append(format(ord(i), '08b'))
        return new_data

    def get_labeled_exif(self, exif):
        labeled = {}
        for (key, val) in exif.items():
            labeled[TAGS.get(key)] = val

        return labeled

    def save_image_data_to_cache(self, image=None):
        img = image if image else self.image_path
        image = Image.open(img, 'r')
        copy = image.copy()
        data = copy.getdata()
        data_array = np.array(data)
        hashed_data = hashlib.sha1(data_array).hexdigest()
        self.cache.set(f'{copy.filename}_value', hashed_data)
        self.cache.set(f'{copy.filename}_exif', self.get_labeled_exif(copy))

    def check_image_cache(self, image):
        if self.cache.get(image.filename):
            return True
        return False

    def mod_pix(self, pix: list, data: str, factor: int, image_size: tuple, video: bool = False):
        """
        Modify a set of pixel values in order to hide data inside of it.
        :param pix: A list of 3-tuples (or 4-tuples if there is an alpha channel) of every pixel value
        in the image.
        :type pix: list
        :param data: The data to be encoded into the image
        :param factor: What factor the loops should be skipped by i.e. factor 2 skips every second loop
        :param image_size: A tuple representing the dimensions of the image
        :param video: A tuple that is True if the object is a Video file. There will use fewer loop cycles
        so as to save time. The full process can take up to 30 minutes for a particularly large file and
        large dataset.
        :return:
        """

        datalist = self.gen_data(data)
        lendata = len(datalist)
        loops = math.ceil(len(pix) / lendata)
        imdata = iter(pix)

        if video:
            loops = math.ceil(loops / 20)

        self.logger.debug(f'LOOPS: {loops}')
        self.logger.debug(f'PIX: {len(pix)}')

        for loop in range(loops - 1):

            if loop == 0:
                loop_start = 0
                loop_end = (lendata * (loop + 1))
            else:
                if video:
                    loop_start = lendata * (loop * 10)
                    loop_end = (lendata * (loop + 1))
                else:
                    loop_start = lendata * (loop + factor - 1)
                    loop_end = (lendata * (loop + factor))

            values = list(pix)[loop_start:loop_end]

            for i, val in enumerate(values):
                try:
                    pixs = [value for value in imdata.__next__()[:3] +
                            imdata.__next__()[:3] +
                            imdata.__next__()[:3]]
                except StopIteration:
                    return

                # Pixel value should be made
                # odd for 1 and even for 0
                for j in range(0, 8):
                    if (datalist[i][j] == '0') and (pixs[j] % 2 != 0):

                        if pixs[j] % 2 != 0:
                            pixs[j] -= 1

                    elif (datalist[i][j] == '1') and (pixs[j] % 2 == 0):
                        pixs[j] -= 1

                # Eighth pixel of every set tells
                # whether to stop or read further.
                # 0 means keep reading; 1 means the
                # message is over.
                if i == lendata - 1:
                    if pixs[-1] % 2 == 0:
                        pixs[-1] -= 1
                else:
                    if pixs[-1] % 2 != 0:
                        pixs[-1] -= 1

                pixs = tuple(pixs)
                yield pixs[0:3]
                yield pixs[3:6]
                yield pixs[6:9]


class Steganography(BaseSteganography):
    """
    An object that can encode data into an image or decode data from an image.

    It uses the LSB (Least Significant Bit) technique in order to hide the information. At a later date I'd like to
    change this and add extra possibilities such as those used by the stegano package plus a few others that are viable.

    If at all possible I'd also like to try and implement a few custom variations of those algorithms.
    """

    def encode_enc(self, newimg: Image, data: str, video: bool = False):
        """
        Takes a copy of an image and encodes the specified data into it by modifying pixels.
        :param newimg:
        :param data:
        :param video:
        :return:
        """

        start_time = time.time()
        w, h = newimg.size

        factor = self.image_factor(w, h)

        self.logger.debug(f'Image is {w}x{h} and factor is {factor}')

        (x, y) = (0, 0)

        for pixel in self.mod_pix(newimg.getdata(), data, factor=factor, image_size=(w, h), video=video):

            # Putting modified pixels in the new image
            newimg.putpixel((x, y), pixel)
            if x == w - 1:
                x = 0
                y += 1
            else:
                x += 1

        end_time = time.time()

        self.logger.debug(f'ENTIRE FUNC: Took {end_time - start_time} seconds')

    def encode_array(self, image_array, text, image_size):

        binary_format_text = self.gen_data(text)
        len_binary_format_text = len(binary_format_text)


        for row in image_array:
            loops = math.ceil(len(row) / len_binary_format_text) - 1

            for pixel in row:
                pass

    def encode(self, text='', image_path='', replace=False, video=False, encryption_key='', integrity=None):
        """
        Takes text, a path to an image and an encryption key to encrypt the data then hide it within the image.
        :param text:
        :param image_path:
        :param replace:
        :param video:
        :param encryption_key:
        :param integrity:
        :return: str
        """
        if text == '' and self.text:
            text = self.text
        elif text != '' and not self.text:
            self.text = text
        elif text != '' and self.text:
            text = text
        else:
            return f'Error: No text is specified either here or on the class definition.'

        if not image_path:
            image_path = self.image_path

        if len(text) == 0:
            raise ValueError('Data is empty')

        image = Image.open(image_path, 'r')

        encryption_engine = Encrypt()
        text, key_file = encryption_engine.encrypt_text(text, encryption_key)

        # Add start point to encrypted text
        text = '++++++++' + text
        # Add end point to encrypted text
        text += '########'

        # array_image = cv2.imread(image_path)
        # self.encode_array(array_image, text, image.size)

        new_image = image.copy()
        # self.cache.set()
        self.encode_enc(new_image, text, video)

        image_name = image.filename.split('/')[-1]

        if replace:
            self.logger.debug(f'REPLACE = {replace} for image {image_path}')
            self.delete_file(image_path)
            new_img_name = f'./static/images/{image_name}'
        else:
            new_img_name = f'./static/images/encoded_{image_name}'

        new_image.save(new_img_name, str(image.format))

        stop_time = time.time()
        width, height = image.size

        pixels_per_second = (width * height) / (stop_time - self.start_time)

        self.logger.debug(f'TOOK: {stop_time - self.start_time} seconds to process {image.size[0] * image.size[1]} pixels')
        self.logger.debug(f'PIXELS PER SECOND: {pixels_per_second}')

        return new_img_name[1:]

    def build_composite_data(self, data, start, end):
        """
        This function does not work currently. It is meant to take sporadic data that has been 'broken' by the images
        being cut or otherwise manipulated. I am currently trying to figure out how best to handle this
        TODO: Fix this - Broken as of 31/07/2020
        :param data:
        :param start:
        :param end:
        :return:
        """
        # Remove all empty space characters
        data = data.replace('ÿ', '')

        start_data = data.split(start)
        end_data = data.split(end)

        if len(start_data) > 1:
            start_data = start_data[1]

        if len(end_data) > 1:
            end_data = end_data[0]
            self.logger.debug(f'END: {end_data}')

        self.logger.debug(f'START: {start_data}')
        return start_data[0]

    def decode(self, image='', encryption_key=''):
        """
        Decode text that has been extracted from an image.
        :param image:
        :param encryption_key:
        :return:
        """
        if image == '' and self.image_path:
            image = self.image_path
        elif image != '' and not self.image_path:
            self.image_path = image
            image = image
        elif image != '' and self.image_path:
            image = image
        else:
            return 'Error: No image has been specified either here or on the class definition.'

        img = Image.open(image, 'r')

        image_name = image.split('/')[-1]

        new_img_name = f'static/images/decoded_{image_name}'

        data = ''
        extracted_data = ''
        imgdata = iter(img.getdata())

        while True:
            try:
                pixels = [value for value in imgdata.__next__()[:3] +
                          imgdata.__next__()[:3] +
                          imgdata.__next__()[:3]]
            except StopIteration:
                self.logger.debug(f'FAILED on {image_name}')
                return False
            # string of binary data
            binstr = ''

            for i in pixels[:8]:
                if i % 2 == 0:
                    binstr += '0'
                else:
                    binstr += '1'

            data += chr(int(binstr, 2))

            if self.start_point in data:
                extracted_data += chr(int(binstr, 2))
                self.logger.debug(f'FOUND: {self.start_point}')
                self.logger.debug(f'EXTRACTED_DATA: {extracted_data}')

                if data[-9:-1] == self.end_point:
                    self.logger.debug(f'FOUND: {self.end_point}')
                    try:
                        extracted_data = extracted_data.lstrip('+')
                        extracted_data = extracted_data.rstrip('########+')

                        self.logger.debug(f'UNENCRYPTED_DATA: {extracted_data}')
                        
                        key = f'./storage/keys/{encryption_key}.key'

                        self.logger.debug(f'KEYFILE: {encryption_key}')

                        encryption_engine = Encrypt()
                        decoded_data = encryption_engine.decrypt_text(text=bytes(extracted_data.encode()), key_file=key)
                        img.save(new_img_name, str(img.format))
                        return new_img_name, decoded_data
                    except cryptography.fernet.InvalidToken:
                        self.logger.debug(f'INVALID TOKEN, CONTINUING')
                        # extracted_data = ''
                        extracted_data = self.build_composite_data(extracted_data, self.start_point, self.end_point)
                        continue
                    except cryptography.exceptions.InvalidSignature:
                        self.logger.debug(f'INVALID SIGNATURE, CONTINUING')
                        # extracted_data = ''
                        self.build_composite_data(extracted_data, self.start_point, self.end_point)
                        continue

    def video_to_frames(self, video_path, frames_dir, overwrite=False, every=1):
        """
        Extracts the frames from a video
        :param video_path: path to the video
        :param frames_dir: directory to save the frames
        :param overwrite: overwrite frames if they exist?
        :param every: extract every this many frames
        :return: path to the directory where the frames were saved, or None if fails
        """

        video_dir, video_filename = os.path.split(video_path)  # get the video path and filename from the path

        # make directory to save frames, its a sub dir in the frames_dir with the video name
        os.makedirs(os.path.join(frames_dir, video_filename.split('.')[0]), exist_ok=True)

        vid_cap = cv2.VideoCapture(video_path)
        i = 0
        while vid_cap.isOpened():
            ret, frame = vid_cap.read()
            if not ret:
                break
            cv2.imwrite(f'{frames_dir}/{video_filename.split(".")[0]}/{i}.jpg', frame)
            i += 1

        vid_cap.release()
        cv2.destroyAllWindows()

        return os.path.join(frames_dir, video_filename.split(".")[0])  # when done return the directory containing the frames

    def frames_to_video(self, frames_dir, video_dir, video_name):
        """
        Converts a directory of images to a video
        :param frames_dir:
        :param video_dir:
        :param video_name:
        :return:
        """
        img_array = []
        size = (0, 0)
        for subdir, dirs, files in os.walk(frames_dir):
            files = [int(frame.split('.')[0]) for frame in files]
            files.sort()
            for filename in files:
                img = cv2.imread(f'{subdir}/{filename}.jpg')
                height, width, layers = img.shape
                size = (width, height)
                img_array.append(img)

        out = cv2.VideoWriter(filename=f'{video_dir}/{video_name}.mp4', fourcc=cv2.VideoWriter_fourcc(*'mp4v'), fps=30,
                              frameSize=size)

        for i in range(len(img_array)):
            out.write(img_array[i])
        out.release()

        return f'{video_dir}/{video_name}.mp4'

    def encode_video(self, video_path, frames_path, text, encryption_key=''):
        """
        Used to encode and hide text in images from a video.
        :param video_path:
        :param frames_path:
        :param text:
        :param encryption_key:
        :return:
        """
        video_frames = self.video_to_frames(video_path, frames_path)

        for subdir, dirs, files in os.walk(video_frames):

            files = [int(frame.split('.')[0]) for frame in files]
            files.sort()

            for i, frame in enumerate(files):
                # Every 25th frame, add the watermark
                if (i + 1) % 250 == 0:
                    self.encode(text, f'{subdir}/{str(frame)}.jpg', replace=True, video=True, encryption_key=encryption_key)

        new_video = self.frames_to_video(video_frames, './static/videos', 'encoded_video')

        return new_video

    def decode_video(self, video_path, frames_path, encryption_key=''):
        """
        Convert video to frames and search for encrypted data within frames.
        :param video_path:
        :param frames_path:
        :param encryption_key:
        :return:
        """
        video_frames = self.video_to_frames(video_path, frames_path)

        self.logger.debug(f'VIDEO_FRAMES: {video_frames}')

        decoded_text = []

        for subdir, dirs, files in os.walk(video_frames):

            files = [int(frame.split('.')[0]) for frame in files]
            files.sort()

            for i, frame in enumerate(files):
                # check for the watermark
                if (i + 1) % 250 == 0:
                    self.logger.debug(f'FRAME: {frame}.jpg')
                    decoded_frame, text = self.decode(f'{subdir}/{str(frame)}.jpg', encryption_key=encryption_key)
                    decoded_text.append(text)
                    break

        new_video = self.frames_to_video(video_frames, './static/videos', 'encoded_video')

        return new_video, decoded_text

    def delete_file(self, file):
        os.remove(file)

    # The below does not work yet - as of July 31st 2020 and should NOT be used.

    def test_image_blocks(self, image_path, replace=False):
        image = Image.open(image_path, 'r')
        new_image = image.copy()

        data = 'This is some text that I will be encoded into the below data'

        encryption_engine = Encrypt()
        text, key_file = encryption_engine.encrypt_text(data, 'master')

        # Add start point to encrypted text
        text = '++++++++' + text

        # Add end point to encrypted text
        text += '########'

        width = new_image.size[0]

        self.get_image_blocks(new_image.getdata(), text, width, video=False)

        image_name = image.filename.split('/')[-1]

        if replace:
            self.logger.debug(f'REPLACE = {replace} for image {image_path}')
            self.delete_file(image_path)
            new_img_name = f'./static/images/{image_name}'
        else:
            new_img_name = f'./static/images/encoded_{image_name}'

        new_image.save(new_img_name, str(image.format))
        return new_img_name

    def get_image_blocks(self, image, data, image_width=0, video=False):
        blocks = []

        start_time = time.time()

        datalist = self.gen_data(data)
        lendata = len(datalist)

        self.logger.debug(f'image_width = {image_width}')
        self.logger.debug(f'lendata = {lendata}')

        line_loops = math.ceil(image_width / lendata)
        whole_loops = math.ceil(len(image) / lendata)
        block_width = math.ceil(image_width / line_loops)

        self.logger.debug(f'block_width: {block_width}')

        if video:
            whole_loops = math.ceil(whole_loops / 20)

        for loop in range(whole_loops - 1):
            block = []

            if loop == 0:
                loop_start = 0
            else:
                if video:
                    loop_start = block_width * (loop * 10)
                else:
                    loop_start = block_width * loop

            values = list(image)[loop_start:block_width * (loop + 1)]

            for item in values:
                block.append(item)
            blocks.append(block)

        (x, y) = (0, 0)

        for block in blocks:
            self.logger.debug(f'BEFORE len(block): {len(block)}')

        for pixel in self.modify_blocks(blocks, datalist, lendata):

            # Putting modified pixels in the new image
            image.putpixel((x, y), pixel)
            if x == image_width - 1:
                x = 0
                y += 1
            else:
                x += 1

        end_time = time.time()

        self.logger.debug(f'ENTIRE FUNC: Took {end_time - start_time} seconds')

    def modify_blocks(self, blocks, datalist, lendata):
        self.logger.debug(f'DATALIST: {datalist}')
        self.logger.debug(f'len(datalist): {len(datalist)}')
        self.logger.debug(f'len(blocks): {len(blocks)}')

        for block in blocks:
            # Extracting 3 pixels at a time
            self.logger.debug(f'AFTER len(block): {len(block)}')
            block = iter(block)
            for i, b in enumerate(block):
                try:
                    pixs = [value for value in block.__next__()[:3] +
                            block.__next__()[:3] +
                            block.__next__()[:3]]

                    # Pixel value should be made
                    # odd for 1 and even for 0
                    for j in range(0, 8):
                        if (datalist[i][j] == '0') and (pixs[j] % 2 != 0):

                            if pixs[j] % 2 != 0:
                                pixs[j] -= 1

                        elif (datalist[i][j] == '1') and (pixs[j] % 2 == 0):
                            pixs[j] -= 1

                    # Eighth pixel of every set tells
                    # whether to stop or read further.
                    # 0 means keep reading; 1 means the
                    # message is over.
                    if i == lendata - 1:
                        if pixs[-1] % 2 == 0:
                            pixs[-1] -= 1
                    else:
                        if pixs[-1] % 2 != 0:
                            pixs[-1] -= 1

                    pixs = tuple(pixs)
                    yield pixs[0:3]
                    yield pixs[3:6]
                    yield pixs[6:9]
                except StopIteration:
                    continue


class LSBSteganography(object):
    def __init__(self):
        self.bitsPerChar = 8
        self.bitsPerPixel = 3
        self.maxBitStuffing = 2
        self.extension = "png"
        self.logger = logging.getLogger('LSBSteganography')
        self.start = '++++++++'
        self.stop = '########'
        self.start_time = time.time()

    def can_encode(self, message, image):
        width, height = image.size
        image_capacity = width * height * self.bitsPerPixel
        message_capacity = (len(message) * self.bitsPerChar) - (self.bitsPerChar + self.maxBitStuffing)
        return image_capacity >= message_capacity

    def image_factor(self, w: int, h: int, integrity: int) -> int:
        """
        Takes the width and height of an image, and the requested integrity, and determines the factor by which to alter
        the rate of encoding.
        :param w: Width of the image in pixels
        :param h: height of the image in pixels
        :param integrity: Level of data integrity required
        """

        percentage = math.ceil((h * (10 * integrity)) / 100)

        self.logger.debug(f'INTEGRITY: {integrity}')
        self.logger.debug(f'PERCENTAGE: {percentage}')

        if w <= 1024:
            factor = (h / percentage) * 2
        elif 1024 < w <= 1280:
            factor = (h / percentage) * 5
        elif 1280 < w <= 1920:
            factor = (h / percentage) * 10
        elif 1920 < w <= 2560:
            factor = (h / percentage) * 20
        elif 2560 < w <= 3840:
            factor = (h / percentage) * 40
        else:
            factor = (h / percentage) * 80

        factor = math.ceil(factor)

        return factor

    def create_binary_triple_pairs(self, message):
        binaries = list(
            "".join([bin(ord(i))[2:].rjust(self.bitsPerChar, '0') for i in message]) + "".join(['0'] * self.bitsPerChar))
        binaries = binaries + ['0'] * (len(binaries) % self.bitsPerPixel)
        binaries = [binaries[i * self.bitsPerPixel:i * self.bitsPerPixel + self.bitsPerPixel] for i in
                    range(0, int(len(binaries) / self.bitsPerPixel))]
        return binaries

    def embed_bits_to_pixels(self, binary_triple_pairs, pixels, integrity: int = 0):
        binary_pixels = [list(bin(p)[2:].rjust(self.bitsPerChar, '0') for p in pixel) for pixel in pixels]
        for i in range(len(binary_triple_pairs)):
            for j in range(len(binary_triple_pairs[i])):
                binary_pixels[i][j] = list(binary_pixels[i][j])
                binary_pixels[i][j][-1] = binary_triple_pairs[i][j]
                binary_pixels[i][j] = "".join(binary_pixels[i][j])

        new_pixels = [tuple(int(p, 2) for p in pixel) for pixel in binary_pixels]
        return new_pixels

    def encode(self, message, image_filename, new_image_filename, integrity: int = 0):
        img = Image.open(image_filename)
        size = img.size

        message = f'{self.start}{message}{self.stop}'

        if not self.can_encode(message, img):
            return None

        binary_triple_pairs = self.create_binary_triple_pairs(message)
        self.logger.debug(f'binary_triple_pairs: {binary_triple_pairs}')

        pixels = list(img.getdata())
        new_pixels = self.embed_bits_to_pixels(binary_triple_pairs, pixels)

        new_image = Image.new("RGB", size)
        new_image.putdata(new_pixels)

        new_image.save(new_image_filename)

        stop_time = time.time()
        width, height = size

        pixels_per_second = (width  * height) / (stop_time - self.start_time)

        self.logger.debug(f'TOOK: {stop_time - self.start_time} seconds to process {size[0]*size[1]} pixels')
        self.logger.debug(f'PIXELS PER SECOND: {pixels_per_second}')

        return new_image_filename

    def get_lsbs_from_pixels(self, binary_pixels):
        total_zeros = 0
        bin_list = []
        for binaryPixel in binary_pixels:
            for p in binaryPixel:
                if p[-1] == '0':
                    total_zeros = total_zeros + 1
                else:
                    total_zeros = 0
                bin_list.append(p[-1])
                if total_zeros == self.bitsPerChar:
                    return bin_list

    def decode(self, image_filename):
        img = Image.open(image_filename)
        pixels = list(img.getdata())
        binary_pixels = [list(bin(p)[2:].rjust(self.bitsPerChar, '0') for p in pixel) for pixel in pixels]
        bin_list = self.get_lsbs_from_pixels(binary_pixels)
        message = "".join([chr(int("".join(bin_list[i:i + self.bitsPerChar]), 2)) for i in
                           range(0, len(bin_list) - self.bitsPerChar, self.bitsPerChar)])
        start = message.find(self.start) + len(self.start)
        end = message.find(self.stop)
        message = message[start:end]

        stop_time = time.time()
        width, height = img.size

        pixels_per_second = (width * height) / (stop_time - self.start_time)

        self.logger.debug(f'TOOK: {stop_time - self.start_time} seconds to process {img.size[0] * img.size[1]} pixels')
        self.logger.debug(f'PIXELS PER SECOND: {pixels_per_second}')

        return f'/{image_filename}', message


class CannyEdgeSteganography(BaseSteganography):
    """
    Honestly not sure if this will work correctly, ever. It relies on hoping the algorithm finds the exact same pixels
    everytime which I am not 100% sure of, so data integrity probability is low.
    TODO: As of Friday 9th October 2020, this has been abandoned. I may pick this up later, but probably not.
    """
    def __init__(self, image_path=None, text=None):
        super(CannyEdgeSteganography, self).__init__()
        self.image_path = image_path
        self.text = text
        self.logger = logging.getLogger('cannyLogger')

    def can_encode(self, text_bits, image_bits):
        return len(text_bits) < len(image_bits)

    def get_image_edge_pixels(self, image=None, image_name=None, show=False):
        edges = cv2.Canny(image, 100, 200)

        edge_pixel_positions = []

        for i, row in enumerate(edges):
            for j, pixel in enumerate(row):
                if pixel != '0':
                    edge_pixel_positions.append([i, j])

        if show:
            plt.subplot(121), plt.imshow(image, cmap='gray')
            plt.title('Original Image'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(edges, cmap='gray')
            plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

            plt.show()

        cv2.imwrite(f'./static/images/canny_{image_name}', edges)

        return edge_pixel_positions, f'/static/images/canny_{image_name}'

    def pixel_to_binary(self, pixel):
        return bin(pixel)

    def encode_pixel(self, pixel, data):
        # Pixel LSB should match the data
        if (data == '0') and (pixel % 2 != 0):

            if pixel % 2 != 0:
                pixel -= 1

        elif (data == '1') and (pixel % 2 == 0):
            pixel -= 1

        return pixel

    def encode(self, image=None, text: str = None):
        text = self.start_point + text + self.end_point
        data = self.gen_data(text)
        data_str_list = [item for item in data]

        self.logger.debug(f'data_str_list: {data_str_list}')

        im_path = image if image else self.image_path
        image_name = im_path.split('/')[-1]

        image_array = cv2.imread(im_path, -1)

        edge_pixel_positions, edge_pixels_image = self.get_image_edge_pixels(image_array, image_name=image_name, show=True)

        edge_pixels = []
        for position in edge_pixel_positions:
            edge_pixels.append(image_array[position[0]][position[1]])

        if not self.can_encode(data_str_list, edge_pixels):
            raise ValueError(f'Data does not fit in the specified image.')

        loops = math.floor(len(edge_pixels) / len(data_str_list))

        for loop in range(loops):

            if loop == 0:
                loop_start = 0
                loop_end = (len(data) * (loop + 1))
            else:
                loop_start = len(data) * (loop - 1)
                loop_end = len(data) * loop

            values = list(edge_pixel_positions)[loop_start:loop_end]

            new_data = ''.join([d for d in data])

            for i, position in enumerate(values):
                pixel = image_array[position[0]][position[1]][0]
                new_pix = self.encode_pixel(pixel, new_data[i])
                image_array[position[0]][position[1]][0] = new_pix

        file_name = image.split('/')[-1]

        cv2.imwrite(f'./static/images/encoded_{file_name}', image_array)

        return f'/static/images/encoded_{file_name}'  # f'./static/images/encoded_{file_name}'

    def decode(self, image):

        im_path = image if image else self.image_path
        image_name = im_path.split('/')[-1]

        image_array = cv2.imread(im_path, -1)

        edge_pixel_positions, edge_pixels_image = self.get_image_edge_pixels(image=image_array, image_name=image_name,
                                                                             show=True)

        edge_pixels = []
        for position in edge_pixel_positions:
            edge_pixels.append(image_array[position[0]][position[1]][0])

        data = ''
        extracted_data = ''

        i = 0

        while True:

            if i == 0:
                start = 0
                end = 8
            else:
                start = i * 8
                end = (i + 1) * 8

            # string of binary data
            binstr = ''

            for j in edge_pixels[start:end]:
                if j % 2 == 0:
                    binstr += '0'
                else:
                    binstr += '1'

            if len(binstr) > 7:
                data += chr(int(binstr, 2))

            try:

                if self.start_point in data:
                    extracted_data += chr(int(binstr, 2))

                    if data[-9:-1] == self.end_point:
                        self.logger.debug(f'FOUND ENDPOINT: {extracted_data}')
                        extracted_data = extracted_data.lstrip('+')
                        extracted_data = extracted_data.rstrip('########+')

                        return extracted_data
            except ValueError:
                return 'No data could be correctly extracted from the image'

            i += 1
