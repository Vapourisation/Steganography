FROM python:3.8
WORKDIR /app
RUN apt-get update
RUN apt install gcc
RUN apt-get install 'ffmpeg'\
    'libsm6'\
    'libxext6'  -y
ENV LIBRARY_PATH=/lib:/usr/lib
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . /app/
CMD ["pytest"]
CMD ["python", "app.py"]