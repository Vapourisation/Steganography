import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY', 'you-will-never-guess')
    UPLOAD_PATH = 'uploads/'
    REDIS = {
        'host': os.environ.get('REDIS_HOST'),
        'port': os.environ.get('REDIS_PORT', '6379'),
    }
