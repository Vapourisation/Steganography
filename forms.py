from wtforms.form import Form
from wtforms import StringField, SubmitField, FileField, SelectField
from wtforms.fields.html5 import IntegerRangeField
from wtforms.validators import DataRequired

images = ['jpg', 'png', 'jpeg']


class EncodeForm(Form):
    text = StringField('Text to encode', validators=[DataRequired()])
    type = SelectField('What type of encoding do you want?', choices=['Simple LSB', 'Complex LSB'],
                       description='Simple LSB is much quicker but less robust, only encoding the data once.'
                                   ' Complex LSB takes much longer but encodes the data through the entire image, '
                                   'ensuring data integrity.')
    integrity = IntegerRangeField('Level of data integrity', default=1)
    image = FileField('Image to encode data in', validators=[DataRequired()])


class DecodeForm(Form):
    image = FileField('Image to decode', validators=[DataRequired()])
    type = SelectField('What type of encoding was used?', choices=['Simple LSB', 'Complex LSB'],
                       description='Simple LSB is much quicker but less robust, only encoding the data once.'
                                   ' Complex LSB takes much longer but encodes the data through the entire image, '
                                   'ensuring data integrity.')
