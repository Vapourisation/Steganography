// Websocket test work
// example: wsTest( `ws://${document.domain}:${location.port}/ws`, 'Hey there!' )
const wsTest = ( endpoint, message ) => {
  let exampleSocket = new WebSocket( endpoint )

  exampleSocket.onopen = () => {
    exampleSocket.send( message )
  }

  exampleSocket.onmessage = ( event ) => {
    console.log( event.data )
  }

  if ( exampleSocket.bufferedAmount === 0 ) {
    exampleSocket.close()
  }
}

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

const decoder = new TextDecoder('utf-8')
let progress = document.getElementById('progress');

// (async () => {
//   let response = await fetch('/progress', {
//     method: "post",
//     credentials: "same-origin",
//     headers: {
//         "X-CSRFToken": getCookie("csrftoken")
//     },
//   })
//
//   let reader = response.body.getReader();
//
//   // Step 2: get total length
//   const contentLength = +response.headers.get('Content-Length');
//
//   // Step 3: read the data
//   let receivedLength = 0; // received that many bytes at the moment
//   let chunks = []; // array of received binary chunks (comprises the body)
//   while(true) {
//     const {done, value} = await reader.read();
//
//     if (done) {
//       break;
//     }
//
//     chunks.push(value);
//     console.log(value)
//     receivedLength += value.length;
//
//     // console.log(`Received ${receivedLength} of ${contentLength}`)
//   }
//
//   // Step 4: concatenate chunks into single Uint8Array
//   let chunksAll = new Uint8Array(receivedLength); // (4.1)
//   let position = 0;
//   for(let chunk of chunks) {
//     chunksAll.set(chunk, position); // (4.2)
//     position += chunk.length;
//   }
//
//   // Step 5: decode into a string
//   let result = decoder.decode(chunksAll);
// })()

let submitEncode = document.getElementById('submit_encode');
let submitDecode = document.getElementById('submit_decode');
let main = document.querySelector('main');
let container = main.querySelectorAll('.container')[0];
let type = document.getElementById('type');

if (type !== null) {
  let info = document.querySelector('.info');
  let simple_lsb = 'Simple LSB is much quicker but less robust, only encoding the data once.';
  let complex_lsb = 'Complex LSB takes much longer but encodes the data through the entire image, ensuring data integrity.';

  type.addEventListener('change', () => {
    if (type.value === 'Simple LSB') {
      info.innerText = simple_lsb;
      info.classList.add('show');
    } else if (type.value === 'Complex LSB') {
      info.innerText = complex_lsb;
      info.classList.add('show');
    }
    else if (type.value === 'Canny Edge LSB') {
      info.innerText = canny_lsb;
      info.classList.add('show');
    } else {
      info.innerText = '';
      info.classList.remove('show');
    }
  })
}

const post_encode = async ( body ) => {
  let newImage = document.createElement('img');
  let downloadButton = document.getElementById('downloadImage');

  newImage.classList.add('encoded');

  fetch('/encode', {
    method: "post",
    credentials: "same-origin",
    headers: {
        "X-CSRFToken": getCookie("csrftoken")
    },
    body: body
  }).then( (response) => {
    return response.json()
  }).then( (data) => {
    console.log(data.resp)

    newImage.src = data.resp;
    downloadButton.href = data.resp;
    downloadButton.removeAttribute('disabled')
    submitEncode.innerHTML = '';
    submitEncode.innerText = 'encode';
    console.log(container);
    console.log(newImage);
    container.append(newImage);
  })
}

const post_decode = async ( body ) => {
  let newImage = document.createElement('img');
  let decodedText = document.createElement('p');
  let downloadButton = document.getElementById('downloadImage');

  newImage.classList.add('decoded');
  decodedText.classList.add('decoded_text');

  fetch('/decode', {
    method: "post",
    credentials: "same-origin",
    headers: {
        "X-CSRFToken": getCookie("csrftoken")
    },
    body: body
  }).then( (response) => {
    return response.json()
  }).then( (data) => {
    console.log(data.img, data.msg)

    newImage.src = data.img;
    decodedText.innerText = data.msg;
    downloadButton.href = data.img;
    downloadButton.removeAttribute('disabled')
    submitDecode.innerHTML = '';
    submitDecode.innerText = 'decode';
    console.log(container);
    console.log(newImage);
    container.append(decodedText);
    container.append(newImage);
  })
}

if (submitDecode !== null) {
  submitDecode.addEventListener('click', () => {
    let loader = document.createElement('p');
    let type = document.getElementById('type');
    let image = document.querySelector('input[type="file"]');

    loader.classList.add('loader');
    submitDecode.innerText = '';
    submitDecode.appendChild(loader);

    let formData = new FormData();
    let type_value = type.value;

    formData.append('type', type_value);
    formData.append('image', image.files[0]);

    post_decode(formData).then(r => console.log(r));
  })
}

if (submitEncode !== null) {
  submitEncode.addEventListener('click', () => {
    let loader = document.createElement('p');
    let text = document.getElementById('text');
    let type = document.getElementById('type');
    let image = document.querySelector('input[type="file"]');
    let integrity = document.querySelector('#integrity');

    console.log(`INTEGRITY: ${integrity.value}`)

    loader.classList.add('loader');
    submitEncode.innerText = '';
    submitEncode.appendChild(loader);

    let formData = new FormData();
    let text_value = text.value;
    let type_value = type.value;
    let integrity_value = integrity.value;

    formData.append('text', text_value);
    formData.append('type', type_value);
    formData.append('integrity', integrity_value);
    formData.append('image', image.files[0]);

    post_encode(formData).then(r => console.log(r));
  })
}

function outputUpdate(integrity) {
    document.querySelector('#selected-integrity').value = integrity;
}