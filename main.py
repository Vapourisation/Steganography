import argparse
import sys

from Steganography import Steganography

if __name__ == '__main__':
    a = argparse.ArgumentParser(description='Encode text to an image or video frames. Decode text from an image or '
                                            'video frames')
    a.add_argument('-a', '--action', help='Action to apply to item e.g. encode or decode')
    a.add_argument('-v', '--video', help='Path to video file')
    a.add_argument('-vf', '--videoFrames', help='Path to frame output directory')
    a.add_argument('-i', '--image', help='Path to desired image')
    a.add_argument('-t', '--text', help='Text to encode')
    a.add_argument('-k', '--key', help='Name for the encryption key')
    args = a.parse_args()
    print(args)

    if args.action == 'encode':
        text = args.text
        if args.image:
            image = Steganography(image_path=args.image, text=text)
            encoded_image = image.encode(text=text, encryption_key=args.key)
            print(f'Encoded image: {encoded_image}')
        elif args.video and args.videoFrames:
            video = Steganography()
            encoded_video = video.encode_video(video_path=args.video, frames_path=args.videoFrames, text=text,
                                               encryption_key=args.key)
            print(f'ENCODED VIDEO: {encoded_video}')

    elif args.action == 'decode':
        if args.image:
            image = Steganography()
            try:
                decoded_image, decoded_text = image.decode(image=args.image, encryption_key=args.key)
                print(f'Decoded image: {decoded_image} \nDecoded text: {decoded_text}')
            except ValueError as e:
                print(f'Received an error, did you specify an image anywhere? \nError: {e}')
            except TypeError as e:
                print(f'ERROR: {e}')
        elif args.video:
            video = Steganography()
            try:
                decoded_image, decoded_text = video.decode_video(video_path=args.video, frames_path=args.videoFrames,
                                                                 encryption_key=args.key)
                print(f'Decoded image: {decoded_image} \nDecoded text: {decoded_text}')
            except ValueError as e:
                print(f'Received an error, did you specify a video anywhere? \nError: {e}')
            except TypeError as e:
                print(f'ERROR: {e}')

    elif args.action == 'test':
        image = Steganography()
        encoded_image = image.test_image_blocks(args.image)
        print(f'ENCODED IMAGE: {encoded_image}')
