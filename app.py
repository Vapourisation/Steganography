import asyncio
import cv2
import io
import redis
import time

from concurrent.futures import ProcessPoolExecutor
from functools import wraps
from logging import getLogger
from PIL import Image
from quart import Quart, request, websocket, send_file
from quart.logging import serving_handler
from quart.templating import render_template
from werkzeug.utils import secure_filename

from Steganography import Steganography, LSBSteganography, CannyEdgeSteganography
from config import Config
from forms import EncodeForm, DecodeForm

app = Quart(__name__, template_folder='templates')
app.config.from_object(Config)

getLogger('quart.app').removeHandler(serving_handler)


cache = redis.Redis(host='redis', port=6379)

# Use later to better filter steganography types
steg_types = {
    'Complex LSB': Steganography(),
    'Simple LSB': LSBSteganography(),
    'Canny Edge LSB': CannyEdgeSteganography()
}


def encode_text(filename, text, type, integrity):
    if type == 'Simple LSB':
        steg = LSBSteganography()
        encoded = steg.encode(text, f'uploads/{filename}', f'static/images/encoded_{filename}')
    elif type == 'Complex LSB':
        steg = Steganography(image_path=f'uploads/{filename}', text=text, integrity=integrity)
        encoded = steg.encode()
    elif type == 'Canny Edge LSB':
        steg = CannyEdgeSteganography()
        encoded = steg.encode(image=f'uploads/{filename}', text=text)
    else:
        raise ValueError('No type argument specified')

    return encoded


def decode_text(filename, type):
    if type == 'Complex LSB':
        steg = Steganography()
        img, message = steg.decode(image=f'uploads/{filename}', encryption_key='master')
    elif type == 'Simple LSB':
        lsb_steg = LSBSteganography()
        img, message = lsb_steg.decode(f'uploads/{filename}')
    elif type == 'Canny Edge LSB':
        canny_steg = CannyEdgeSteganography()
        img, message = canny_steg.decode(f'uploads/{filename}')
    else:
        raise ValueError('No type argument specified')

    return img, message


@app.route('/')
async def index():
    data = {'title': f'Welcome to Spark! Steganography.', 'body': 'Would you like to Encode or Decode an image?'}
    return await render_template('index.html', page_data=data)


@app.route('/decode', methods=['GET'])
async def decode_page():
    form = DecodeForm()

    return await render_template('decode.html', form=form)


@app.route('/decode', methods=['POST'])
async def decode_post():
    data = await request.form
    files = await request.files

    image_data = files['image'].read()
    filename = secure_filename(files['image'].filename)

    image = Image.open(io.BytesIO(image_data))
    image.save(f'{app.config["UPLOAD_PATH"]}/{filename}')

    # result = asyncio.get_running_loop().run_in_executor(None, decode_text, filename, data['type'])
    img, message = decode_text(filename=filename, type=data['type'])

    return {'img': img, 'msg': message}


@app.route('/encode', methods=['GET'])
async def encode():
    form = EncodeForm()

    return await render_template('encode.html', form=form)


@app.route('/encode', methods=['POST'])
async def encode_post():
    data = await request.form
    files = await request.files

    image_data = files['image'].read()
    filename = secure_filename(files['image'].filename)

    image = Image.open(io.BytesIO(image_data))
    image.save(f'{app.config["UPLOAD_PATH"]}/{filename}')

    # result = asyncio.get_running_loop().run_in_executor(None, encode_text, filename, data['text'], data['type'], data['integrity'])
    result = encode_text(filename=filename, text=data['text'], type=data['type'], integrity=data['integrity'])

    return {'resp': result}


@app.route('/progress', methods=['POST'])
async def progress():
    length = 100  # len(bytes(''.join([str(i) for i in range(100)])))
    print(f'LENGTH: {length}')

    # async def generate():
    #     for i in range(100):
    #         yield bytes(i)
    #         time.sleep(2)
    # return generate(), 200, {'Content-Type': 'text/html', 'Content-Length': length}
    return {1: 100}


@app.route('/videos/<string:video_name>')
async def chunked_video(video_name):
    resp = await send_file(f'./static/videos/{video_name}')
    await resp.make_conditional(request.range, max_partial_size=100_000)
    return resp


# Testing Websockets here
connected_websockets = set()


def collect_websocket(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        global connected_websockets
        queue = asyncio.Queue()
        connected_websockets.add(queue)
        try:
            return await func(queue, *args, **kwargs)
        finally:
            connected_websockets.remove(queue)
    return wrapper


async def broadcast(message):
    print(f'RUN - broadcast with: {message}')
    for queue in connected_websockets:
        await queue.put(message)


@app.websocket('/ws')
async def ws():
    while True:
        data = await websocket.receive()
        await websocket.send(f"Hello Websocket, pleased to meet you too")


@app.websocket("/socket")
@collect_websocket
async def socket_test(queue):
    while True:
        data = await queue.get()
        await websocket.send(data)


if __name__ == '__main__':
    print(f'OpenCV has OpenCL? - {cv2.ocl.haveOpenCL()}')
    app.run(host='0.0.0.0', port=5000, debug=True)
