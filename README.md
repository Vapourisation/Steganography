# Steganography
A simple Steganography package with a web interface. Previously it used Flask but I have since changed it to Quart for async support.

_NOTE: CURRENTLY VIDEO SUPPORT IS MINIMAL AND DOES NOT WORK CORRECTLY_

## Running in Docker
`docker-compose up --build` - Will build and run the program locally using port 5000

## CLI application
This will require a virtual environment or, if you're feeling spicy, install all packages locally:
`pip install -r requirements.txt`

The possible arguments are:
* `-a --action`         The action you want to perform (decode/encode)
* `-v --video`          Full path to video file you want to encode/decode
* `-vf --videoFrames`   Full path to frame output directory
* `-i --image`          Full path to the image file you want to encode/decode
* `-t --text`           Some text object you want to encode to the image/video
* `-k --key`            The name of the key used to perform the encryption operation on the text

After this you run from the command-line as follows:

##### Encode
`python main.py -a=encode -i=<path-to-image> -t='Some text to encode to the image' -k=someKeyName`

##### Decode
`python main.py -a=decode -i=<path-to-image> -k=someKeyName`
