import base64
import os

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend


class Encrypt:
    def __init__(self):
        pass

    def generate_encryption_key(self, name):
        file_name = f'storage/keys/{name}.key'
        if not os.path.exists(file_name):
            print(f'NOT FOUND: {file_name}')
            salt = os.urandom(16)
            key = Fernet.generate_key()
            kdf = PBKDF2HMAC(
                algorithm=hashes.SHA512(),
                length=32,
                salt=salt,
                iterations=100000,
                backend=default_backend()
            )
            key = base64.urlsafe_b64encode(kdf.derive(key))

            with open(file_name, 'wb') as f:
                f.write(key)
        else:
            print(f'FOUND: {file_name}')

        return file_name

    def load_key(self, key):
        """
        Loads the key from the current directory named `key.key`
        """
        import time

        i = 0
        while i < 5:
            try:
                file = open(key, "rb").read()
                break
            except FileNotFoundError:
                time.sleep(5)
                i += 1
        else:
            raise FileNotFoundError(f'Key file: {key}')
        return file

    def encrypt_text(self, text, key_file=''):

        text = text.encode()
        name = key_file if key_file else 'master'
        key_file = self.generate_encryption_key(name)

        key = self.load_key(key_file)
        f = Fernet(key)

        encrypted = f.encrypt(text)

        key_file_name = key_file.split('/')[-3:]
        key_file = '/'.join(key_file_name)
        print(key_file)

        print(f'ENCRYPTED: {encrypted}')

        return encrypted.decode(), key_file

    def decrypt_text(self, text, key_file=''):
        key_file = key_file if key_file else 'storage/keys/master.key'
        key = self.load_key(key_file)
        f = Fernet(key)

        decrypted = f.decrypt(text)

        print(f'DECRYPTED: {decrypted}')

        return decrypted.decode()
